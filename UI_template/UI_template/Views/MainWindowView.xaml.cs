﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using UI_template.Helpers;

namespace UI_template.Views
{
    public partial class MainWindowView : Window
    {
        private string _activeButtonName;

        public MainWindowView()
        {
            InitializeComponent();
            SetSelectedButton(ReadmeButton);
        }

        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            AnimateMouseOver(sender, 0.3, 0.7, 300);
        }

        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            AnimateMouseOver(sender, 0.7, 0.3, 300);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;

            if (button != null)
            {
                SetSelectedButton(button);
            }
        }

        #region -- Animation --

        private void AnimateMouseOver(object sender, double from, double to, int duration)
        {
            var button = sender as Button;
            var buttonContent = button.Content as TextBlock;

            if (button != null)
            {
                var background = button.Background as ImageBrush;
                AnimateOpacity(background, from, to, duration);

                if (from < to)
                {
                    AnimateWidth(button, 200, 210, 100);
                    AnimateHeight(button, 100, 110, 100);
                    AnimateFontSize(buttonContent, 19, 21, 100);
                }
                else
                {
                    AnimateWidth(button, 210, 200, 500);
                    AnimateHeight(button, 110, 100, 500);
                    AnimateFontSize(buttonContent, 21, 19, 500);
                }
            }
        }

        private void AnimateOpacity(ImageBrush background, double from, double to, int duration)
        {
            background.BeginAnimation(Brush.OpacityProperty, ViewHelpers.CreateDoubleAnimation(from, to, duration));
        }

        private void AnimateWidth(Button button, double from, double to, int duration)
        {
            button.BeginAnimation(WidthProperty, ViewHelpers.CreateDoubleAnimation(from, to, duration));
        }

        private void AnimateHeight(Button button, double from, double to, int duration)
        {
            button.BeginAnimation(HeightProperty, ViewHelpers.CreateDoubleAnimation(from,to,duration));
        }

        private void AnimateFontSize(TextBlock buttonContent, double from, double to, int duration)
        {
            var textBlockMaxWidthAnimation = from < to ? ViewHelpers.CreateDoubleAnimation(130, 140, duration) : ViewHelpers.CreateDoubleAnimation(140, 130, duration);
            
            buttonContent.BeginAnimation(MaxWidthProperty, textBlockMaxWidthAnimation);
            buttonContent.BeginAnimation(FontSizeProperty, ViewHelpers.CreateDoubleAnimation(from, to, duration));
        }

        #endregion -- Animation --

        #region -- Tab Selection --

        private void SetSelectedButton(Button button)
        {
           ((TextBlock)button.Content).Foreground = new SolidColorBrush(Colors.Brown);

            ResetPreviouslySelectedButton();
            _activeButtonName = button.Name;
        }

        private void ResetPreviouslySelectedButton()
        {
            switch (_activeButtonName)
            {
                case "GridsButton":
                    ((TextBlock)GridsButton.Content).Foreground = new SolidColorBrush(Colors.Black);
                    break;
                case "ChartsButton":
                    ((TextBlock)ChartsButton.Content).Foreground = new SolidColorBrush(Colors.Black);
                    break;
                case "FormsButton":
                    ((TextBlock)FormsButton.Content).Foreground = new SolidColorBrush(Colors.Black);
                    break;
                //case "EventsButton":
                //    ((TextBlock)EventsButton.Content).Foreground = new SolidColorBrush(Colors.Black);
                //    break;
                case "ReadmeButton":
                    ((TextBlock)ReadmeButton.Content).Foreground = new SolidColorBrush(Colors.Black);
                    break;
            }
        }

        #endregion -- Tab Selection --

    }
}
