﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UI_template.Views.Grids
{
    public partial class LowerGridView : UserControl
    {
        public LowerGridView()
        {
            InitializeComponent();
        }

        private void CheckBox_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            //Grid.Items.OfType<ItemModel>().ToList().ForEach(x => x.IsSelected = true);
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            //Grid.Items.OfType<ItemModel>().ToList().ForEach(x => x.IsSelected = false);
        }
    }
}
