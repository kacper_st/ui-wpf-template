﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace UI_template.Views.Charts
{

    public partial class ChartsTabView : UserControl
    {
        public ChartsTabView()
        {
            InitializeComponent();
        }

        private void StackPanel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var stackPanel = sender as StackPanel;

            if (stackPanel.Name == "UpperStackPanel")
            {
                SwitchUpperPanel();
            }
            else if (stackPanel.Name == "LowerStackPanel")
            {
                SwitchLowerPanel();
            }
        }

        private void SwitchUpperPanel()
        {
            if (UpperPanelExtendabilityIndicator.Text == "+")
            {
                UpperRow.Height = new GridLength(1, GridUnitType.Auto);
                UpperGrid.Visibility = Visibility.Collapsed;
                UpperPanelExtendabilityIndicator.Text = "-";
            }
            else
            {
                UpperRow.Height = new GridLength(1, GridUnitType.Star);
                UpperGrid.Visibility = Visibility.Visible;
                UpperPanelExtendabilityIndicator.Text = "+";
            }
        }

        private void SwitchLowerPanel()
        {
            if (LowerPanelExtendabilityIndicator.Text == "+")
            {
                LowerRow.Height = new GridLength(1, GridUnitType.Auto);
                LowerGrid.Visibility = Visibility.Collapsed;
                LowerPanelExtendabilityIndicator.Text = "-";
            }
            else
            {
                LowerRow.Height = new GridLength(1, GridUnitType.Star);
                LowerGrid.Visibility = Visibility.Visible;
                LowerPanelExtendabilityIndicator.Text = "+";
            }
        }
    }
}
