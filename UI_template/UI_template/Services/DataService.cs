﻿using System.Collections.Generic;
using UI_template.Models;

namespace UI_template.Services
{
    public class DataService
    {
        public List<ItemModel> GetItemsList()
        {
            List<ItemModel> list = new List<ItemModel>();

            ItemModel model1 = new ItemModel
            {
                ID = 101,
                Amount = 10,
                IsAvailable = true,
                Description = "You awaken aboard Talos I, a space station orbiting the moon in the year 2032. You are the key subject of an experiment meant to alter humanity forever – but things have gone terribly wrong.",
                Name = "Prey",
                Price = (float)(149.99),
                ItemImage = @"\Images\Prey.jpg"
            };
            list.Add(model1);

            ItemModel model2 = new ItemModel
            {
                ID = 126,
                Amount = 0,
                IsAvailable = false,
                Description = "NieR: Automata tells the story of androids 2B, 9S and A2 and their battle to reclaim the machine-driven dystopia overrun by powerful machines.",
                Name = "NieR: Automata",
                Price = (float)(199.99),
                ItemImage = @"\Images\Nier.jpg"
            };
            list.Add(model2);

            ItemModel model3 = new ItemModel
            {
                ID = 189,
                Amount = 13,
                IsAvailable = true,
                Description = "Get ready for the mind blowing insanity! Play as one of four trigger-happy mercenaries and take out everything that stands in your way! With its addictive action, frantic first - person shooter combat, massive arsenal of weaponry, RPG elements and four - player co - op, Borderlands is a breakthrough experience that challenges all the conventions of modern shooters.",
                Name = "Borderlands",
                Price = (float)(39.99),
                ItemImage = @"\Images\Borderlands.jpg"
            };
            list.Add(model3);

            return list;
        }

        public List<TransactionModel> GetTransactionList()
        {
            List<TransactionModel> list = new List<TransactionModel>();

            TransactionModel model1 = new TransactionModel
            {
                TransactionID = 101,
                Amount = 1,
                Date = new System.DateTime(2017, 5, 1, 13, 23, 00),
                User = "user1@gmail.com",
                UserID = 1,
                ItemID = 101,
                ItemName = "Prey"
            };
            list.Add(model1);

            TransactionModel model2 = new TransactionModel
            {
                TransactionID = 102,
                Amount = 2,
                Date = new System.DateTime(2017, 5, 3, 13, 23, 00),
                User = "user2@gmail.com",
                UserID = 2,
                ItemID = 126,
                ItemName = "NieR: Automata"
            };
            list.Add(model2);

            TransactionModel model3 = new TransactionModel
            {
                TransactionID = 103,
                Amount = 1,
                Date = new System.DateTime(2017, 3, 1, 11, 23, 00),
                User = "user3@gmail.com",
                UserID = 3,
                ItemID = 101,
                ItemName = "Prey"
            };
            list.Add(model3);

            TransactionModel model4 = new TransactionModel
            {
                TransactionID = 104,
                Amount = 2,
                Date = new System.DateTime(2017, 5, 5, 13, 23, 00),
                User = "user4@gmail.com",
                UserID = 4,
                ItemID = 126,
                ItemName = "NieR: Automata"
            };
            list.Add(model4);

            TransactionModel model5 = new TransactionModel
            {
                TransactionID = 105,
                Amount = 3,
                Date = new System.DateTime(2017, 5, 7, 13, 23, 00),
                User = "user5@gmail.com",
                UserID = 5,
                ItemID = 189,
                ItemName = "Borderlands"
            };
            list.Add(model5);

            TransactionModel model6 = new TransactionModel
            {
                TransactionID = 106,
                Amount = 1,
                Date = new System.DateTime(2017, 5, 7, 13, 23, 00),
                User = "user6@gmail.com",
                UserID = 6,
                ItemID = 101,
                ItemName = "Prey"
            };
            list.Add(model6);

            TransactionModel model7 = new TransactionModel
            {
                TransactionID = 107,
                Amount = 1,
                Date = new System.DateTime(2017, 5, 10, 13, 23, 00),
                User = "user7@gmail.com",
                UserID = 7,
                ItemID = 189,
                ItemName = "Borderlands"
            };
            list.Add(model7);

            TransactionModel model8 = new TransactionModel
            {
                TransactionID = 108,
                Amount = 1,
                Date = new System.DateTime(2017, 5, 12, 13, 23, 00),
                User = "user8@gmail.com",
                UserID = 8,
                ItemID = 189,
                ItemName = "Borderlands"
            };
            list.Add(model8);

            TransactionModel model9 = new TransactionModel
            {
                TransactionID = 109,
                Amount = 2,
                Date = new System.DateTime(2017, 5, 14, 13, 23, 00),
                User = "user9@gmail.com",
                UserID = 9,
                ItemID = 101,
                ItemName = "Prey"
            };
            list.Add(model9);

            return list;
        }

        public List<DayModel> GetDayList()
        {
            List<DayModel> list = new List<DayModel>();

            for (int i = 1; i <= 31; i++)
            {
                DayModel model = new DayModel
                {
                    Label = i.ToString(),
                    DayNumber = i
                };
                list.Add(model);
            }

            return list;
        }
    }
}
