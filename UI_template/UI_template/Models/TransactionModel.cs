﻿using System;

namespace UI_template.Models
{
    public class TransactionModel
    {
        public int TransactionID { get; set; }
        public int UserID { get; set; }
        public string User { get; set; }
        public int Amount { get; set; }
        public DateTime Date { get; set; }
        public int ItemID { get; set; }
        public string ItemName { get; set; }
    }
}
