﻿namespace UI_template.Models
{
    public class DayModel
    {
        public int Value { get; set; }
        public string Label { get; set; }
        public int DayNumber { get; set; }
    }
}
