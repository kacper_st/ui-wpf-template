﻿namespace UI_template.Models
{
    public class ItemModel : SelectableModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Amount { get; set; }
        public float Price { get; set; }
        public string ItemImage { get; set; }
        public bool IsAvailable { get; set; }
    }
}
