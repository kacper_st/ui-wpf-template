﻿using Caliburn.Micro;
using UI_template.ViewModels.Charts;
using UI_template.ViewModels.Forms;
using UI_template.ViewModels.Grids;
using UI_template.ViewModels.Readme;

namespace UI_template.ViewModels
{
    public class MainWindowViewModel : Screen
    {

        #region -- Properties --

        private Screen _activeTab;
        public Screen ActiveTab
        {
            get { return _activeTab; }
            set
            {
                _activeTab = value;
                NotifyOfPropertyChange(() => ActiveTab);
            }
        }

        #endregion -- Properties --

        #region -- ViewModels --

        public GridsTabViewModel GridsVM { get; set; }
        public ChartsTabViewModel ChartsVM { get; set; }
        public FormsTabViewModel FormsVM { get; set; }
        public ReadmeTabViewModel ReadmeVM { get; set; }

        #endregion -- ViewModels --

        #region -- Buttons --

        public void GridsButton()
        {
            GridsVM = new GridsTabViewModel();
            ActiveTab = GridsVM;
        }

        public void ChartsButton()
        {
            ChartsVM = new ChartsTabViewModel();
            ActiveTab = ChartsVM;
        }

        public void FormsButton()
        {
            FormsVM = new FormsTabViewModel();
            ActiveTab = FormsVM;
        }

        public void ReadmeButton()
        {
            ReadmeVM = new ReadmeTabViewModel();
            ActiveTab = ReadmeVM;
        }

        #endregion -- Buttons --

        public MainWindowViewModel()
        {
            DisplayName = "UI template";
            ReadmeButton();
        }

    }
}
