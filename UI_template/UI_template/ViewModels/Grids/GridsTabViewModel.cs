﻿using Caliburn.Micro;

namespace UI_template.ViewModels.Grids
{
    public class GridsTabViewModel : Screen
    {
        #region -- ViewModels --

        public LowerGridViewModel LowerGridVM { get; set; }
        public UpperGridViewModel UpperGridVM { get; set; }

        #endregion -- ViewModels --



        public GridsTabViewModel()
        {
            LowerGridVM = new LowerGridViewModel();
            NotifyOfPropertyChange(() => LowerGridVM);

            UpperGridVM = new UpperGridViewModel();
            NotifyOfPropertyChange(() => UpperGridVM);
        }
    }
}
