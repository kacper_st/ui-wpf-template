﻿using Caliburn.Micro;
using System.Collections.Generic;
using UI_template.Models;
using UI_template.Services;

namespace UI_template.ViewModels.Grids
{
    public class LowerGridViewModel : Screen
    {
        DataService service;

        #region -- Properties --

        public List<TransactionModel> ItemsSource { get; set; }

        #endregion

        public LowerGridViewModel()
        {
            Initialize();
            LoadData();
        }

        private void Initialize()
        {
            service = new DataService();
            ItemsSource = new List<TransactionModel>();
        }

        private void LoadData()
        {
            ItemsSource = service.GetTransactionList();
            NotifyOfPropertyChange(() => ItemsSource);
        }
    }
}
