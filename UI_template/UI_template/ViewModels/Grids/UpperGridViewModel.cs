﻿using Caliburn.Micro;
using System.Collections.Generic;
using UI_template.Models;
using UI_template.Services;

namespace UI_template.ViewModels.Grids
{
    public class UpperGridViewModel : Screen
    {
        DataService service;

        #region -- Properties --

        public List<ItemModel> ItemsSource { get; set; }

        #endregion

        public UpperGridViewModel()
        {
            Initialize();
            LoadData();
        }

        private void Initialize()
        {
            service = new DataService();
            ItemsSource = new List<ItemModel>();
        }

        private void LoadData()
        {
            ItemsSource = service.GetItemsList();
            NotifyOfPropertyChange(() => ItemsSource);
        }
    }
}
