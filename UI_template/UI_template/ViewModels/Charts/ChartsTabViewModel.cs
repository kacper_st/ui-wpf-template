﻿using Caliburn.Micro;

namespace UI_template.ViewModels.Charts
{
    public class ChartsTabViewModel : Screen
    {
        public BarChartsViewModel BarChartVM { get; set; }
        public PieChartsViewModel PieChartVM { get; set; }


        public ChartsTabViewModel()
        {
            BarChartVM = new BarChartsViewModel();
            NotifyOfPropertyChange(() => BarChartVM);

            PieChartVM = new PieChartsViewModel();
            NotifyOfPropertyChange(() => PieChartVM);
        }
    }
}
