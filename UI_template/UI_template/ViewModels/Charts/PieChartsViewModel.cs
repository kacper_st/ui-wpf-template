﻿using Caliburn.Micro;
using OxyPlot;
using OxyPlot.Series;
using System.Collections.Generic;
using System.Linq;
using UI_template.Helpers;
using UI_template.Models;
using UI_template.Services;

namespace UI_template.ViewModels.Charts
{
    public class PieChartsViewModel : Screen
    {
        DataService service;

        public PlotModel Model { get; set; }

        public PieChartsViewModel()
        {
            service = new DataService();
            GenerateChart();
        }

        private void GenerateChart()
        {
            Model = CreatePlotModel();
            NotifyOfPropertyChange(() => Model);

            SetDataSeries();
            Model.InvalidatePlot(true);
        }

        private PlotModel CreatePlotModel()
        {
            var plot = new PlotModel
            {
                Title = "Sales by items",
                Subtitle = "May, 2017",
                SubtitleColor = OxyColors.Brown,
                SubtitleFontSize = 15,
                SubtitleFontWeight = FontWeights.Bold
            };

            return plot;
        }

        private void SetDataSeries()
        {
            for (int i = 1; i <= 3; i++)
            {
                var series = CreateSeries(i);
                if (series.Any(e => e.Value > 0))
                {
                    Model.Series.Add(CreatePieSeries(series));
                }
            }
        }

        private List<DayModel> CreateSeries(int iterator)
        {
            var data = service.GetTransactionList();
            int itemID = ViewModelHelpers.GetItemByIterator(iterator);

            List<DayModel> list = service.GetDayList();

            foreach (var item in list)
            {
                item.Value = data.Where(x => x.ItemID == itemID && x.Date.Day == item.DayNumber).Sum(x => x.Amount);
            }

            var outputList = list.Where(x => x.Value > 0).ToList();
            return outputList;
        }

        private PieSeries CreatePieSeries(IEnumerable<DayModel> values)
        {
            PieSeries series = new PieSeries
            {
                StrokeThickness = 2.0,
                InsideLabelPosition = 0.8,
                AngleSpan = 360,
                StartAngle = 0,
                OutsideLabelFormat = "",
                TickHorizontalLength = 0.00,
                TickRadialLength = 0.00
            };

            foreach (var model in values)
            {
                if (model.Value > 0)
                {
                    series.Slices.Add(new PieSlice(model.Label.ToString(), model.Value));
                }
            }

            return series;
        }
    }
}
