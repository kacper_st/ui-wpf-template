﻿using Caliburn.Micro;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System.Collections.Generic;
using System.Linq;
using UI_template.Helpers;
using UI_template.Models;
using UI_template.Services;

namespace UI_template.ViewModels.Charts
{
    public class BarChartsViewModel : Screen
    {
        DataService service;

        public PlotModel Model { get; set; }

        public BarChartsViewModel()
        {
            service = new DataService();
            GenerateChart();
        }

        private void GenerateChart()
        {
            Model = CreateColumnModel();
            NotifyOfPropertyChange(() => Model);

            SetDataSeries();
            Model.InvalidatePlot(true);
        }

        private PlotModel CreateColumnModel()
        {
            var plot = new PlotModel
            {
                Title = "Sales statistics",
                Subtitle = "May, 2017",
                SubtitleColor = OxyColors.Brown,
                SubtitleFontSize = 15,
                SubtitleFontWeight = FontWeights.Bold,
                LegendPosition = LegendPosition.RightTop,
                LegendBorderThickness = 1,
                LegendBackground = OxyColor.FromAColor(180, OxyColors.White),
                LegendBorder = OxyColors.Black
            };

            plot.Axes.Add(new CategoryAxis
            {
                ItemsSource = service.GetDayList(),
                LabelField = "Label",
                IsZoomEnabled = false,
                IsPanEnabled = false,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Dot,
                MinorGridlineColor = OxyColors.LightGray
            });

            plot.Axes.Add(new LinearAxis
            {
                Position = AxisPosition.Left,
                MinimumPadding = 0,
                AbsoluteMinimum = 0,
                IsZoomEnabled = false,
                IsPanEnabled = false,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Dot,
                MinorGridlineColor = OxyColors.LightGray,
                MinimumMinorStep = 1,
                MinimumMajorStep = 1
            });

            return plot;
        }

        private void SetDataSeries()
        {
            for (int i = 1; i <= 3; i++)
            {
                var series = CreateSeries(i);
                if (series.Any(e => e.Value > 0))
                {
                    Model.Series.Add(CreateColumnSeries(ViewModelHelpers.GetItemNameByIterator(i), series));
                }
            }
        }

        private List<DayModel> CreateSeries(int iterator)
        {
            var data = service.GetTransactionList();
            int itemID = ViewModelHelpers.GetItemByIterator(iterator);

            List<DayModel> list = service.GetDayList();

            foreach (var item in list)
            {
                item.Value = data.Where(x => x.ItemID == itemID && x.Date.Day == item.DayNumber).Sum(x => x.Amount);
            }

            return list;
        }

        private ColumnSeries CreateColumnSeries(string title, IEnumerable<DayModel> values)
        {
            return new ColumnSeries { Title = title, ItemsSource = values, ValueField = "Value" };
        }
    }
}
