﻿namespace UI_template.Helpers
{
    public static class ViewModelHelpers
    {
        public static int GetItemByIterator(int iterator)
        {
            if (iterator == 1)
            {
                return 101;
            }
            else if (iterator == 2)
            {
                return 126;
            }
            else
            {
                return 189;
            }
        }

        public static string GetItemNameByIterator(int iterator)
        {
            if (iterator == 1)
            {
                return "Prey";
            }
            else if (iterator == 2)
            {
                return "NieR: Automata";
            }
            else
            {
                return "Borderlands";
            }
        }
    }
}
