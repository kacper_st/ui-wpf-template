﻿using System;
using System.Windows.Media.Animation;

namespace UI_template.Helpers
{
    public static class ViewHelpers
    {
        public static DoubleAnimation CreateDoubleAnimation(double from, double to, int duration)
        {
            DoubleAnimation animation = new DoubleAnimation();
            animation.From = from;
            animation.To = to;
            animation.Duration = TimeSpan.FromMilliseconds(duration);

            return animation;
        }
    }
}
